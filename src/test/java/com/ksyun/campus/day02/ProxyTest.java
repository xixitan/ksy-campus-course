package com.ksyun.campus.day02;

import com.ksyun.campus.day02.proxy.IUserDao;
import com.ksyun.campus.day02.proxy.demo.CglibProxyFactory;
import com.ksyun.campus.day02.proxy.demo.JdkProxyFactory;
import com.ksyun.campus.day02.proxy.impl.UserDao;
import com.ksyun.campus.day02.proxy.demo.UserDaoProxy;
import org.junit.Test;


public class ProxyTest {


    @Test
    public void testStaticProxy() {
        IUserDao userDao = new UserDao();
        IUserDao staticProxy = new UserDaoProxy(userDao);
        staticProxy.save();
    }

    @Test
    public void testJdkProxy() {
        IUserDao userDao = new UserDao();
        JdkProxyFactory proxyFactory = new JdkProxyFactory(userDao);
        IUserDao jdkProxy = (IUserDao) proxyFactory.getProxyInstance();
        jdkProxy.save();
    }

    @Test
    public void testCglibProxy() {
        IUserDao userDao = new UserDao();
        CglibProxyFactory proxyFactory = new CglibProxyFactory(userDao);
        IUserDao cglibProxy = (IUserDao) proxyFactory.getProxyInstance();
        cglibProxy.save();

    }
}
