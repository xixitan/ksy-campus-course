package com.ksyun.campus.day02;


import com.ksyun.campus.day02.annotation.model.Dept;
import com.ksyun.campus.utils.PoiUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.io.File;
import java.util.List;


@Slf4j
public class AnnotationTest {

    @Test
    public void testAnnotationParse() {
        List<Dept> deptList = Dept.getDefaultDeptList();
        String filePath = System.getProperty("user.dir") + File.separator + System.currentTimeMillis() + ".xlsx";
        log.info("filePath:{}", filePath);
        PoiUtil.createExcelLocal(filePath, deptList);
    }
}
