package com.ksyun.campus.day02;

import com.ksyun.campus.utils.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Slf4j
public class StreamTest {

    /**
     * Stream对像获取
     */
    @Test
    public void testStreamCreate() {
        //通过集合对象的stream()方法
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
        Stream stream = list.stream();
        Stream parallelStream = list.parallelStream();
        //通过数组对象
        Stream arryaStream = Arrays.stream(new Integer[]{1, 2, 3, 4, 5});
        //通过Stream静态方法
        Stream staticMethod = Stream.of(1, 2, 3, 4, 5);
    }

    @Test
    public void testStreamMidOperation() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
        list.forEach(x -> System.out.println(x));
        List<String> strs = list.stream().map(x -> String.valueOf(x)).collect(Collectors.toList());
        List<Integer> odd = list.stream().filter(x -> x % 2 == 1).collect(Collectors.toList());
        List<Integer> limit = list.stream().limit(2).collect(Collectors.toList());
        List<Integer> sorted = list.stream().sorted().collect(Collectors.toList());
        List<Integer> distincted = list.stream().distinct().collect(Collectors.toList());
        List<Integer> skipted = list.stream().skip(2).collect(Collectors.toList());
        List<Integer> picked = list.stream().peek(x -> System.out.println(x)).collect(Collectors.toList());
        System.out.println("origin:" + JsonUtils.toJson(list));
        System.out.println("strs:" + JsonUtils.toJson(strs));
        System.out.println("odd:" + JsonUtils.toJson(odd));
        System.out.println("limit:" + JsonUtils.toJson(limit));
        System.out.println("sorted:" + JsonUtils.toJson(sorted));
        System.out.println("distincted:" + JsonUtils.toJson(distincted));
        System.out.println("skipted:" + JsonUtils.toJson(skipted));
        System.out.println("picked:" + JsonUtils.toJson(picked));
    }

    @Test
    public void testStreamEndOperation() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
        //收集成集合
        List<Integer> listRs = list.stream().collect(Collectors.toList());
        Set<Integer> setRs = list.stream().collect(Collectors.toSet());
        Map<Integer, Integer> mapRs = list.stream().collect(Collectors.toMap(x -> x, y -> y));
        Object[] arrayRs = list.stream().toArray();
        //数据分组
        Map<Boolean, List<Integer>> groupMap = list.stream().collect(Collectors.groupingBy(x -> x % 2 == 0));
        //分组求合
        Map<Boolean, Integer> groupAndSum = list.stream().collect(Collectors.groupingBy(x -> x % 2 == 0, Collectors.summingInt(x -> x)));
        //分组取最大
        Map<Boolean, Integer> groupAndMax = list.stream().collect(Collectors.groupingBy(x -> x % 2 == 0,
                Collectors.collectingAndThen(Collectors.maxBy(Integer::compare), Optional::get)));
    }


}
