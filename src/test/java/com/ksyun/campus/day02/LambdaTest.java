package com.ksyun.campus.day02;

import com.ksyun.campus.day02.annotation.model.Dept;
import com.ksyun.campus.day02.lambda.LambdaMethod;
import com.ksyun.campus.utils.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class LambdaTest {


    /**
     * lambda demo
     */
    @Test
    public void lambdaDemo() {
        //1.无参，无返回值
        LambdaMethod.run(() -> System.out.println("noArgs,no result,just print"));
        //2.有参，无返回值（可选的参数圆括号、可选的方法体大括号）
        LambdaMethod.consume("Hello Java", x -> System.out.println("accept param:" + x));
        LambdaMethod.consume(10, (x) -> {
            System.out.println("accept param:" + x);
        });
        //3.无参，有返回值 (可选的返回关键字)
        Object result = LambdaMethod.supplier(() -> {
            return "generage my lambda";
        });
        System.out.println(result);
        result = LambdaMethod.supplier(() -> "generage by lambda");
        System.out.println(result);

        //4.from T To R
        Integer rs = (Integer) LambdaMethod.multiply(1, a -> (Integer) a * 5);
        System.out.println(rs);
        //5.from T1,T2 To R
        rs = (Integer) LambdaMethod.sum(1, 2, (a, b) -> (Integer) a + (Integer) b);
        System.out.println(rs);
        //可选的参数类型声明
        rs = LambdaMethod.sum(1, 2, 3, (Integer a, Integer b, Integer c) -> a + b + c);
        System.out.println(rs);
        rs = LambdaMethod.sum(1, 2, 3, (a, b, c) -> a + b + c);
        System.out.println(rs);
    }


    public static void process(Runnable r) {
        r.run();
    }

    @Test
    public void testRunnable() {
        //使用Lambda，以内联的形式为函数式接口的抽象方法提供实现，并把整个表达式作为函数式接口的实例
        Runnable r1 = () -> System.out.println("Hello World 1");
        //匿名类
        Runnable r2 = new Runnable() {
            public void run() {
                System.out.println("Hello World 2");
            }
        };
        process(r1);
        process(r2);
        //利用直接传递的 Lambda 打印 "Hello World 3"
        process(() -> System.out.println("Hello World 3"));
    }


    /**
     * 匿名内部类实现
     */
    @Test
    public void testFunctionInterfaceWithClass() {
        List<Integer> nums = Arrays.asList(1, 9, 3, 5);
        Collections.sort(nums, new Comparator<Integer>() {
            @Override
            public int compare(Integer a, Integer b) {
                return a.compareTo(b);
            }
        });
        System.out.println(JsonUtils.toJson(nums));
    }

    /**
     * 函数式接口实现
     */
    @Test
    public void testFunctionInterfaceWithLambda() {
        List<Integer> nums = Arrays.asList(1, 9, 3, 5);
        Collections.sort(nums, (a, b) -> {
            return a.compareTo(b);
        });
        System.out.println(JsonUtils.toJson(nums));
    }

    @Test
    public void testFunctionQuote() {
        //静态方法引用
        Comparator<Integer> c2 = (x, y) -> Integer.compare(x, y);
        Comparator<Integer> c1 = Integer::compare;
        //实例方法引用
        List<Dept> depts = Dept.getDefaultDeptList();
        depts.forEach(x -> x.printName());
        depts.forEach(Dept::printName);
        //构造方法引用
        List<Integer> depNumbers = Arrays.asList(1, 2, 3, 5);
        List<Dept> deptsA = depNumbers.stream().map(x -> new Dept(x)).collect(Collectors.toList());
        List<Dept> deptsB = depNumbers.stream().map(Dept::new).collect(Collectors.toList());
    }


}
