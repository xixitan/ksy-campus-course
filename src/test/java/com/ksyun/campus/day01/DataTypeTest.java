package com.ksyun.campus.day01;

import com.ksyun.campus.day02.annotation.model.DataType;
import com.ksyun.campus.utils.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

@Slf4j
public class DataTypeTest {

    @Test
    public void testDataType() {
        DataType dataType = new DataType();
        log.info("default data:{}", JsonUtils.toPrettyJson(dataType));
        DataType maxValue = DataType.getDefaultMax();
        log.info("max data:{}", JsonUtils.toPrettyJson(maxValue));
        DataType minValue = DataType.getDefaultMin();
        log.info("min data:{}", JsonUtils.toPrettyJson(minValue));
    }

    @Test
    public void testAutoOperate() {
        int a = 1;
        int b = a++;
        int c = ++a;
        log.info("a:{},b:{},c:{}", a, b, c);
    }


    @Test
    public void testCompareOperate() {
        int a = 1;
        boolean b = a > 1;
        log.info("b:{}", a > 1);
    }

    /**
     *
     */
    @Test
    public void testLogicOperate() {
        int a = 1;
        int b = 1;
        boolean c = a-- > 0 || b-- > 0;
        log.info("a:{},b:{},c:{}", a, b, c);
        int d = 1;
        int e = 1;
        boolean f = --d > 1 || --e > 1;
        log.info("d:{},e:{},f:{}", d, e, f);
    }

    @Test
    public void testMultiArray() {
        int[][] pairArray = {{1, 2, 3}, {4, 5, 6}};
        log.info("pairArray[0][0][0]:{}", pairArray[0][1]);
        int[][][] tribleArray = {{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}, {{10, 11, 12}}};
        log.info("tribleArray[0][0][0]:{}", tribleArray[0][2][2]);
        log.info("tribleArray[0][0][0]:{}", tribleArray[0][2][3]);
    }
}
