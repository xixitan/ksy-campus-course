package com.ksyun.campus.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.Validate;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Objects;

public class JsonUtils<T> {
    private static final ObjectMapper mapper = new ObjectMapper();

    static {
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
        // 设置输入时忽略在JSON字符串中存在但Java对象实际没有的属性
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    }

    public static String toJson(Object object) {
        try {
            return mapper.writeValueAsString(object);
        } catch (IOException e) {
            throw new RuntimeException("write to json string error:" + Objects.toString(object), e);
        }
    }

    public static <T> T fromJson(String jsonString, Class<T> clazz) {
        Validate.notEmpty(jsonString);

        try {
            return mapper.readValue(jsonString, clazz);
        } catch (IOException e) {
            throw new RuntimeException(jsonString, e);
        }
    }

    public static <T> T fromJson(String jsonString, JavaType javaType) {
        Validate.notEmpty(jsonString);
        try {
            return mapper.readValue(jsonString, javaType);
        } catch (IOException e) {
            throw new RuntimeException(jsonString, e);
        }
    }

    public static <T> T fromJson(String jsonString, TypeReference valueTypeRef) {
        if (jsonString == null || jsonString.trim().length() == 0) {
            return null;
        }
        try {
            return (T) mapper.readValue(jsonString, valueTypeRef);
        } catch (IOException e) {
            throw new RuntimeException(jsonString, e);
        }
    }


    /**
     * 输出JSONP格式数据.
     */
    public static String toPrettyJson(Object object) {
        try {
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("to pretty json error", e);
        }
    }


}
