package com.ksyun.campus.utils;

import com.ksyun.campus.day02.annotation.ExcelAnnotation;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class PoiUtil {

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static <T> void createExcelLocal(String filePath, List<T> sheets) {
        if (CollectionUtils.isEmpty(sheets)) {
            return;
        }
        // 创建一个输出流对象
        OutputStream out = null;
        try {
            //文件输出
            if (StringUtils.isEmpty(filePath)) {
                filePath = System.getProperty("user.home") + File.separator + "excel" + File.separator + "data.xlsx";
            }
            File file = new File(filePath);
            file.getParentFile().mkdirs();
            if (!file.exists()) {
                file.createNewFile();
            }
            XSSFWorkbook workbook = buildWorkBook(sheets);
            out = new FileOutputStream(file);
            workbook.write(out);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(out);
        }
    }


    private static <T> XSSFWorkbook buildWorkBook(List<T> datas) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        XSSFWorkbook workbook = new XSSFWorkbook();
        Class clazz = datas.get(0).getClass();
        String sheetName = clazz.getSimpleName();
        if (clazz.isAnnotationPresent(ExcelAnnotation.class)) {
            ExcelAnnotation anno = (ExcelAnnotation) clazz.getAnnotation(ExcelAnnotation.class);
            sheetName = anno.name();
        }
        XSSFSheet sheet = workbook.createSheet(sheetName);
        // 产生表格标题行
        int rowId = 0;
        XSSFRow row = sheet.createRow(rowId);
        // 获取属性,生成表头
        Field[] fields = datas.get(0).getClass().getDeclaredFields();
        XSSFCellStyle headStyle = getHeadStyle(workbook);
        XSSFCellStyle dataStyle = getDataStyle(workbook);
        for (int i = 0; i < fields.length; i++) {
            Field field = fields[i];
            if (field.isAnnotationPresent(ExcelAnnotation.class)) {
                // 获取该字段的注解对象
                ExcelAnnotation anno = field.getAnnotation(ExcelAnnotation.class);
                XSSFCell cell = row.createCell(i);
                cell.setCellStyle(headStyle);
                XSSFRichTextString text = new XSSFRichTextString(anno.name());
                cell.setCellValue(text);
                sheet.setColumnWidth(i, anno.width() * 256);
            }
        }
        rowId++;
        // 遍历集合数据，产生数据行
        for (T model : datas) {
            row = sheet.createRow(rowId);
            // 获取该类 并获取自身方法
            for (int i = 0; i < fields.length; i++) {
                Field field = fields[i];
                if (field.isAnnotationPresent(ExcelAnnotation.class)) {
                    XSSFCell cell = row.createCell(i);
                    cell.setCellStyle(dataStyle);
                    String methodName = "get" + field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1);
                    Method method = clazz.getMethod(methodName);
                    Object value = method.invoke(model);
                    setCellValue(cell, value);
                }
            }
            rowId++;
        }
        return workbook;
    }

    private static void setCellValue(XSSFCell cell, Object value) {
        if (value == null) {
            return;
        }
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Float) {
            cell.setCellValue((Float) value);
        } else if (value instanceof Double) {
            cell.setCellValue((Double) value);
        } else if (value instanceof Long) {
            cell.setCellValue((Long) value);
        } else if (value instanceof BigDecimal) {
            cell.setCellValue(((BigDecimal) value).doubleValue());
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else if (value instanceof Date) {
            cell.setCellValue(sdf.format(value));
        } else {
            XSSFRichTextString richString = new XSSFRichTextString(value.toString());
            cell.setCellValue(richString);
        }
        return;
    }


    /**
     * 描述：设置表头样式
     */
    private static XSSFCellStyle getHeadStyle(XSSFWorkbook workbook) {
        // 生成一个样式
        XSSFCellStyle style = workbook.createCellStyle();
        // 设置列名样式
        style.setFillForegroundColor(IndexedColors.LEMON_CHIFFON.getIndex());
        style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
        style.setBorderBottom(XSSFCellStyle.BORDER_THIN);
        style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
        style.setBorderRight(XSSFCellStyle.BORDER_THIN);
        style.setBorderTop(XSSFCellStyle.ALIGN_CENTER);
        style.setAlignment(XSSFCellStyle.ALIGN_LEFT);
        // 生成一个字体
        XSSFFont font = workbook.createFont();
        font.setColor(IndexedColors.BLACK.getIndex());
        font.setFontHeightInPoints((short) 10);
        font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
        // 把字体应用到当前的样式
        style.setFont(font);
        return style;
    }

    /**
     * 设置数据样式
     */
    private static XSSFCellStyle getDataStyle(XSSFWorkbook workbook) {
        // 生成并设置内容样式
        XSSFCellStyle style = workbook.createCellStyle();
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
        style.setBorderBottom(XSSFCellStyle.BORDER_THIN);
        style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
        style.setBorderRight(XSSFCellStyle.BORDER_THIN);
        style.setBorderTop(XSSFCellStyle.BORDER_THIN);
        style.setAlignment(XSSFCellStyle.ALIGN_LEFT);
        style.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
        // 生成另一个字体
        XSSFFont font2 = workbook.createFont();
        font2.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);
        // 把字体应用到当前的样式
        style.setFont(font2);
        return style;
    }

}
