package com.ksyun.campus.advanced.JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

// 该demo只是演示通过代码查询mysql数据库。
public class Test {
    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver"; // 数据库驱动的具体实现类

    // 数据库链接
    private static final String DB_URL = "jdbc:mysql://localhost:3306/test_db?characterEncoding=utf8&useSSL=false";

    private static final String USER = "root"; // 数据库用户名

    private static final String PASSWORD = "aaabbb"; // 数据库密码

    public static void main(String[] args) {
        String sql = "select name from student where id = ?";
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            Class.forName(JDBC_DRIVER); // 初始化资源
            conn = DriverManager.getConnection(DB_URL, USER, PASSWORD); //获取数据库链接对象
            ps = conn.prepareStatement(sql); // 获取PreparedStatement对象
            ps.setInt(1, 2); // 给第一个？问号(参数下标，这里只有一个参数)填值，我们取id为2的name值
            ResultSet rs = ps.executeQuery(); // 执行SQL
            while (rs.next()) {
                String paramName = rs.getString("name");
                System.out.println("name: " + paramName); // 结果展示
            }
            rs.close(); //关闭资源
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    conn.close();
                }
            } catch (SQLException se) {
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }

    }
}

