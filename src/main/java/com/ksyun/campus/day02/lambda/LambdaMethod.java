package com.ksyun.campus.day02.lambda;

import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class LambdaMethod {

    public static Object sum(Object paramA, Object paramB, BiFunction biFunction) {
        return biFunction.apply(paramA, paramB);
    }

    public static Object multiply(Object param, Function function) {
        return function.apply(param);
    }

    public static Integer sum(Integer a, Integer b, Integer c, TrebleSum trebleSum) {
        return trebleSum.sum(a, b, c);
    }

    public static void run(Runnable runnable) {
        runnable.run();
    }

    public static void consume(Object param, Consumer consumer) {
        consumer.accept(param);
    }

    public static Object supplier(Supplier supplier) {
        return supplier.get();
    }

}
