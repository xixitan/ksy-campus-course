package com.ksyun.campus.day02.lambda;


@FunctionalInterface
public interface TrebleSum {
    Integer sum(Integer a, Integer b, Integer c);
}
