package com.ksyun.campus.day02.proxy.impl;


import com.ksyun.campus.day02.proxy.IUserDao;

public class UserDao implements IUserDao {
    @Override
    public void save() {
        System.out.println("保存数据");
    }
}