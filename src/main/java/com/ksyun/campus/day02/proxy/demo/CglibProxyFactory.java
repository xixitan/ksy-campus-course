package com.ksyun.campus.day02.proxy.demo;



import net.sf.cglib.proxy.Callback;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

public class CglibProxyFactory {
  	// 维护一个目标对象
    private Object target;
    public CglibProxyFactory(Object target) {
        this.target = target;
    }
    // 为目标对象生成代理对象
    public Object getProxyInstance() {
       //创建一个增强器
        Enhancer enhancer = new Enhancer();
        //设置父类  也就是被代理的类
        enhancer.setSuperclass(target.getClass());
        //设置代理逻辑
        enhancer.setCallbacks(new Callback[]{new MethodInterceptor() {
            @Override
            public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable 			{
                System.out.println("开启事务");
                Object result = methodProxy.invokeSuper(o,objects);
                System.out.println("提交事务");
                return result;
            }
        }});
        //创建代理类
        return enhancer.create();
    }
}  