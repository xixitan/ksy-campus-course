package com.ksyun.campus.day02.proxy.demo;


import com.ksyun.campus.day02.proxy.IUserDao;

public class UserDaoProxy implements IUserDao {
    private IUserDao target;
    public UserDaoProxy(IUserDao target) {
        this.target = target;
    }
    
    @Override
    public void save() {
        System.out.println("开启事务");   
        target.save();
        System.out.println("提交事务");
    }
}