package com.ksyun.campus.day02.proxy;

public interface IUserDao {
    public void save();
}