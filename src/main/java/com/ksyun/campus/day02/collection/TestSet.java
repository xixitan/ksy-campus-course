package com.ksyun.campus.day02.collection;

import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeSet;


public class TestSet {
    public static void main(String[] args) {
        HashSet<String> hashSet = new HashSet<>();
        TreeSet<String> treeSet = new TreeSet<>();
        for (int i=0;i<10;i++){
            hashSet.add(String.format("%s_string",i));
            treeSet.add(String.format("%s_string",i));
        }
        Iterator<String> iterator = hashSet.iterator();
        //迭代的顺序与add顺序无关
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        Iterator<String> treeIterator = treeSet.iterator();
        //迭代顺序与add相同
        while (treeIterator.hasNext()){
            System.out.println(treeIterator.next());
        }

    }
}
