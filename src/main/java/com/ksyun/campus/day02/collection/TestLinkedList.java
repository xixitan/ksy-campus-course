package com.ksyun.campus.day02.collection;

import java.util.LinkedList;


public class TestLinkedList {
    public static void main(String[] args) {
        LinkedList<Integer> queue = new LinkedList<Integer>();
        LinkedList<Integer> dqueue = new LinkedList<Integer>();
        for(int i=0;i<10;i++){
            queue.offer(i);
            dqueue.offerFirst(i);
        }
        for(int i=0;i<10;i++){
            System.out.println(queue.poll());
        }
        for(int i=0;i<10;i++){
            System.out.println(dqueue.pollFirst());
        }
    }
}
