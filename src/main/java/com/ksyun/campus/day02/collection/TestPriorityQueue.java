package com.ksyun.campus.day02.collection;

import java.util.Comparator;
import java.util.PriorityQueue;

public class TestPriorityQueue {
    public static void main(String[] args) {
        //使用默认排序
        PriorityQueue<String> stringQueue = new PriorityQueue<>();

        stringQueue.add("blueberry");
        stringQueue.add("apple");
        stringQueue.add("cherry");

        String first = stringQueue.poll();
        String second = stringQueue.poll();
        String third = stringQueue.poll();

        System.out.println(String.format("%s %s %s",first,second,third));
        //根据字符长度排序
        PriorityQueue<String> compareQueue = new PriorityQueue<>((x,y)->x.length()-y.length());

        compareQueue.add("blueberry");
        compareQueue.add("apple");
        compareQueue.add("cherry");

        first = compareQueue.poll();
        second = compareQueue.poll();
        third = compareQueue.poll();

        System.out.println(String.format("%s %s %s",first,second,third));
    }
}
