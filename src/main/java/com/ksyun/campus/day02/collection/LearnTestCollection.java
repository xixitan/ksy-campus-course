package com.ksyun.campus.day02.collection;

import java.util.*;

/**
 * LearnTestCollection
 *
 * @author jiangran@kingsoft.com
 * @date 2023/3/6
 */
public class LearnTestCollection {
    public static void main(String[] args) {
        String str = "aababcabcdabcde";
        useHashMap(str);
        useArrayList(str);
    }
    public static void useHashMap(String str){
        HashMap<String,Integer> map = new HashMap();
        String[] chars = str.split("");
        for (int i=0;i<chars.length;i++){
            if(map.containsKey(chars[i])){
                map.put((String) chars[i],map.get(chars[i])+1);
            }else {
                map.put((String) chars[i],1);
            }
        }
        for (Map.Entry<String,Integer> e : map.entrySet()){
            System.out.println(String.format("%s:%s",e.getKey(),e.getValue()));
        }
    }
    public static void useArrayList(String str){
        HashMap<String,Integer> map = new HashMap();
        ArrayList<String> arrayList = new ArrayList(Arrays.asList(str.split("")));
        arrayList.sort(Comparator.naturalOrder());
        for(int i = 0;i<arrayList.size();i++){
            int count = 1;
            for(int j = i+1;j<arrayList.size();j++){
                if(arrayList.get(i).equals(arrayList.get(j))){
                    count=count+1;
                    i++;
                }
            }
            System.out.println(String.format("%s:%s", arrayList.get(i),count));
        }
    }
}
