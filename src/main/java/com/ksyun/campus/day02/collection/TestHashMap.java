package com.ksyun.campus.day02.collection;

import java.util.HashMap;
import java.util.Map;


public class TestHashMap {
    public static void main(String[] args) {
        HashMap<String,Integer> map= new HashMap();
        map.put("zhangsan",7);
        map.put("lisi",5);
        map.put("wangwu",10);
        //通过keyset遍历
        for(String key:map.keySet()){
            System.out.println(String.format("key %s value %s",key,map.get(key)));
        }
        //通过entryset遍历
        for(Map.Entry<String,Integer> entry : map.entrySet()){
            System.out.println(String.format("key %s value %s",entry.getKey(),entry.getValue()));
        }
    }
}
