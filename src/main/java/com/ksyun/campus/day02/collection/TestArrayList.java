package com.ksyun.campus.day02.collection;

import java.util.ArrayList;
import java.util.ListIterator;


public class TestArrayList {
    public static void main(String[] args) {
        ArrayList<Integer> list =  new ArrayList<Integer>();
        for (int i=0;i<10;i++){
            list.add(i);
        }
        ArrayList<Integer> list_new =  new ArrayList<Integer>();
        for (int i=10;i<20;i++){
            list_new.add(i);
        }
        list.addAll(list_new);

        //通过迭代器遍历
        ListIterator<Integer> listIterator = list.listIterator();
        while (listIterator.hasNext()){
            System.out.println(listIterator.next());
            //遍历同时进行修改会报ConcurrentModificationException
//            list.remove(listIterator.nextIndex());
            listIterator.remove();//使用ListIterator 则可以在遍历过程中进行修改
        }
        for (Integer i:list) {
            System.out.println(i);
        }
    }
}
