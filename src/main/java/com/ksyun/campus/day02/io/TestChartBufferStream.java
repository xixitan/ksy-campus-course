package com.ksyun.campus.day02.io;

import java.io.*;


public class TestChartBufferStream {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream("/tmp/testfile.txt"),"gbk"));
        BufferedWriter bw = new BufferedWriter(
                new OutputStreamWriter(new FileOutputStream("")));
        String line;
        while((line = br.readLine())!=null){
            System.out.println(line);
            bw.write(line);
            bw.newLine();
            bw.flush();
        }
        br.close();
        bw.close();
    }
}
