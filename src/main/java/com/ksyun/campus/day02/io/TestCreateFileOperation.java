package com.ksyun.campus.day02.io;

import java.io.File;
import java.io.IOException;

public class TestCreateFileOperation {
    public static void main(String[] args) throws IOException {
        File fileDir = new File("/tmp/file_operations");
        fileDir.mkdir();
        File logFile = new File("/tmp/file_operations/error.log");
        Boolean isCreateSuccess = logFile.createNewFile();
        System.out.println(isCreateSuccess ? "Create file successfully!" : "Create file failed");
    }
}
