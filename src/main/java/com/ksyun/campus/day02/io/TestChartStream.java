package com.ksyun.campus.day02.io;

import java.io.*;


public class TestChartStream {
    public static void main(String[] args) throws IOException {
        FileInputStream in = new FileInputStream("/tmp/testfile.txt");
        InputStreamReader isr = new InputStreamReader(in,"gbk");
        FileOutputStream out = new FileOutputStream("/tmp/testfile_dest.txt");
        OutputStreamWriter osw = new OutputStreamWriter(out,"gbk");
        char[] buffer = new char[8*1024];
        int c;
        while ((c= isr.read(buffer,0,buffer.length))!=-1){
            String s = new String(buffer,0,c);
            System.out.println(s);
            osw.write(buffer,0,c);
            osw.flush();
        }
        isr.close();
        osw.close();
    }
}
