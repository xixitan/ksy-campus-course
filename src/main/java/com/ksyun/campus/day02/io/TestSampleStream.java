package com.ksyun.campus.day02.io;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


public class TestSampleStream {
    public static void main(String[] args) throws IOException {
        FileReader fr = new FileReader("/tmp/testfile.txt");
        FileWriter fw = new FileWriter("/tmp/testfile_dest.txt");
        char[] buffer = new char[4096];
        int c;
        while ((c=fr.read(buffer,0,buffer.length))!=-1){
            fw.write(buffer,0,c);
            System.out.println(new String(buffer,0,c));
            fw.flush();
        }
        fr.close();
        fw.close();
    }
}
