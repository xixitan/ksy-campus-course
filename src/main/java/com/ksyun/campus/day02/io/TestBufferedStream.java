package com.ksyun.campus.day02.io;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;


public class TestBufferedStream {
    public static void main(String[] args) {
        String srcFile = "/tmp/testfile.txt";
        String destFile = "/tmp/testfile_dest.txt";
        try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(
                srcFile))) {
            try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(destFile))) {
                byte byteData;
                while ((byteData = (byte) bis.read()) != -1) {
                    bos.write(byteData);
                    System.out.print((char) byteData);
                }
                bos.flush();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
