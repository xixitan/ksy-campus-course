package com.ksyun.campus.day02.io;

import java.io.File;
import java.io.IOException;


public class TestRenameFileOperation {
    public static void main(String[] args) throws IOException {
        File file = new File("/tmp/file");
        Boolean isCreateSuccess = file.createNewFile();
        if(isCreateSuccess){
            boolean isSuccess = file.renameTo(new File("/tmp/file_new"));
            System.out.println(isSuccess ? "Rename file folder successfully!" : "rename file failed");
            System.out.println("the origin file path is "+file.getAbsolutePath());
        }

    }
}
