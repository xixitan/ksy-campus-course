package com.ksyun.campus.day02.io;

import java.io.File;
import java.io.FileOutputStream;

public class TestFileOutputStream {
    public static void main(String[] args) {
        String destFile = "/tmp/testfile.txt";
        // Get the line separator for the current platform
        String lineSeparator = System.getProperty("line.separator");
        String line1 = "test";
        String line2 = "test1";
        String line3 = "test2";
        String line4 = "test3";

        try (FileOutputStream fos = new FileOutputStream(destFile)) {
            fos.write(line1.getBytes());
            fos.write(lineSeparator.getBytes());
            fos.write(line2.getBytes());
            fos.write(lineSeparator.getBytes());
            fos.write(line3.getBytes());
            fos.write(lineSeparator.getBytes());
            fos.write(line4.getBytes());
            // Flush the written bytes to the file
            fos.flush();
            System.out.println("Text has  been  written to "
                    + (new File(destFile)).getAbsolutePath());
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
