package com.ksyun.campus.day02.io;

import org.apache.commons.lang3.RandomUtils;

import java.io.*;

/**
 * LearnTestIO
 *
 * @author jiangran@kingsoft.com
 * @date 2023/3/6
 */
public class LearnTestIO {
    public static void main(String[] args) throws IOException {
        int dirLevel = 3;
        String rootPath = "~/Documents/temp/testio";
        createFiles(3,rootPath);
        copyDir(rootPath,"~/Documents/temp/dir_dest");
    }
    public static void createFiles(int level, String path) throws IOException {
        File file1 = new File(String.format("%s%s_level_%s",path,"/file1",level));
        if(!file1.exists()){
            file1.createNewFile();
        }
        try(FileOutputStream fileOutputStream = new FileOutputStream(file1)) {
            for(int i=0;i<1024*1024;i++){
                int random = RandomUtils.nextInt(0, 10);
                fileOutputStream.write(random);
            }
            fileOutputStream.flush();
        }
        File file2 = new File(String.format("%s%s_level_%s",path,"/file2",level));
        if(!file2.exists()){
            file2.createNewFile();
        }
        try(BufferedOutputStream bos=new BufferedOutputStream(new FileOutputStream(file2))){
            for(int i=0;i<1024*1024;i++){
                int random = RandomUtils.nextInt(0, 10);
                bos.write(random);
            }
            bos.flush();
        }
        File dir = new File(String.format("%s%s_level_%s",path,"/child",level));
        if(!dir.exists()){
            dir.mkdir();
        }
        if (level>1){
            createFiles(level-1, dir.getAbsolutePath());
        }
    }
    public static void copyDir(String sourcePath,String newPath) {
        try {
            (new File(newPath)).mkdirs();
            File fileList = new File(sourcePath);
            String[] strName = fileList.list();
            File temp = null;//游标
            for (int i = 0; i < strName.length; i++) {
                if (sourcePath.endsWith(File.separator)) {
                    temp = new File(sourcePath + strName[i]);
                } else {
                    temp = new File(sourcePath + File.separator + strName[i]);
                }
                if (temp.isFile()) {
                    FileInputStream in = new FileInputStream(temp);
                    File file = new File(newPath + "/" + temp.getName().toString());
                    FileOutputStream out = new FileOutputStream(file);
                    byte[] buffer = new byte[1024 * 8];
                    int length;
                    while ((length = in.read(buffer)) != -1) {

                        out.write(buffer, 0, length);
                    }
                    out.flush();
                    out.close();
                    in.close();
                }
                if (temp.isDirectory()) {
                    copyDir(sourcePath + "/" + strName[i], newPath + "/" + strName[i]);
                }
            }
        } catch (Exception e) {
            System.out.println("文件夹复制失败!");
        }
    }
}
