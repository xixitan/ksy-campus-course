package com.ksyun.campus.day02.io;

import java.io.File;


public class TestDeleteFileOperation {
    public static void main(String[] args) {
        File dir = new File("/tmp/file_operations");
        boolean isSuccessD = dir.delete();
        System.out.println(isSuccessD ? "Delete dir successfully!" : "dir is not empty delete failed");
        File fileDir = new File("/tmp/file_operations/error.log");
        boolean isSuccessF = fileDir.delete();
        System.out.println(isSuccessF ? "Delete file successfully!" : "delete file failed");
        isSuccessD = dir.delete();
        System.out.println(isSuccessD ? "Delete dir successfully!" : "dir is not empty delete failed");

    }
}
