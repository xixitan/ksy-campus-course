package com.ksyun.campus.day02.generics;

public class TestGenericsCollection {
    public static void main(String[] args) {
        String[] strArray=new String[2];
        GenericsCollection<String> strList = new GenericsCollection<>(strArray);
        strList.add("AAA",0);
        strList.add("BBB",1);
//        strList.add(123,3); //have error
        String firstElement = strList.get(0);
        System.out.println(firstElement);
    }
}
