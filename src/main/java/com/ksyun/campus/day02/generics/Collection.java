package com.ksyun.campus.day02.generics;

public class Collection {
    public Collection(int size) {
        elements=new Object[size];
    }
    private Object[] elements;
    public Object get(int index) {
        return elements[index];
    }
    public void add(Object obj,int index) {
        elements[index] = obj;
    }
    public int size() {
        return elements.length;
    }
}
