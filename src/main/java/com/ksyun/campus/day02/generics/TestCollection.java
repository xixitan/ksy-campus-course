package com.ksyun.campus.day02.generics;

public class TestCollection {
    public static void main(String[] args) {
        Collection collection = new Collection(2);
        collection.add("zhangsan",0);
        collection.add(18,1);
        System.out.println(collection.get(0)); // zhangsan
        System.out.println(collection.get(1)); // 18
    }
}
