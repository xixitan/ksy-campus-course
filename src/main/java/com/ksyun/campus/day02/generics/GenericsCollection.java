package com.ksyun.campus.day02.generics;

public class GenericsCollection <E>{
    public GenericsCollection(E[] elements) {
        this.elements = elements;
    }
    private E[] elements;
    public E get(int index) {
        return elements[index];
    }
    public void add(E obj,int index) {
        elements[index] = obj;
    }
    public int size() {
        return elements.length;
    }
}
