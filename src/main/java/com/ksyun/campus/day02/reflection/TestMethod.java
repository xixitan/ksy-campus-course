package com.ksyun.campus.day02.reflection;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class TestMethod {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Class cls = Cat.class;
        Method crow = cls.getMethod("crow", String.class);
        System.out.println(crow);
        Method eat = cls.getDeclaredMethod("eat", String.class);
        System.out.println(eat);

        crow.invoke(new Animal(), "我们一起喵喵叫！");
        eat.invoke(new Cat(), "吃个老鼠！");
    }
}
