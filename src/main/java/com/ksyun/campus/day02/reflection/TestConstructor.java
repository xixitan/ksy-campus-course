package com.ksyun.campus.day02.reflection;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class TestConstructor {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException,
            IllegalAccessException, InstantiationException {
        Class cls = Cat.class;
        Method crow = cls.getMethod("crow", String.class);
        Cat cat = Cat.class.getDeclaredConstructor(int.class, int.class).newInstance(2, 4);
        crow.invoke(cat, "我们一起喵喵叫！");
    }
}
