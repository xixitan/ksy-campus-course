package com.ksyun.campus.day02.reflection;

public class Cat extends Animal{
    public int weight;
    private int age;

    public Cat() {
    }

    public Cat(int weight, int age) {
        this.weight = weight;
        this.age = age;
    }

    public void eat(String str) {
        System.out.println(str);
    }
}
