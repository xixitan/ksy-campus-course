package com.ksyun.campus.day02.reflection;

import java.lang.reflect.Field;

public class TestClass {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        Class cls = Cat.class;
        Field name = cls.getField("name");
        System.out.println(name);
        Field[] fields = cls.getDeclaredFields();
        for (Field f : fields){
            System.out.println(f);
        }
        Cat cat = new Cat();
        cat.name = "wangcai";
        Class catClass = cat.getClass();
        Field catClassField = catClass.getField("name");
        System.out.println(catClassField.get(cat));
        name.set(cat, "huahua");
        System.out.println(name.get(cat));
    }
}
