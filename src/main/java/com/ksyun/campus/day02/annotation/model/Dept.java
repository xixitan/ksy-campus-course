package com.ksyun.campus.day02.annotation.model;

import com.ksyun.campus.day02.annotation.ExcelAnnotation;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;


@Data
@NoArgsConstructor
@ExcelAnnotation(name = "部门信息")
public class Dept {

    @ExcelAnnotation(order = 1, name = "部门号")
    private Integer deptno;
    @ExcelAnnotation(order = 2, name = "部门名")
    private String dname;
    @ExcelAnnotation(order = 3, name = "地址", width = 38)
    private String loc;
    @ExcelAnnotation(order = 4, name = "创建时间", width = 30)
    private Date date;


    public Dept(int depNo, String depName, String loc, Date date) {
        this.deptno = depNo;
        this.dname = depName;
        this.loc = loc;
        this.date = date;
    }

    public Dept(int deptno) {
        this.deptno = deptno;
    }


    public static List<Dept> getDefaultDeptList() {
        List list = new ArrayList();
        for (int i = 0; i < 10; i++) {
            list.add(new Dept(i, "name:" + i, "loc:" + UUID.randomUUID(), new Date()));
        }
        return list;
    }

    public void printName() {
        System.out.println(dname);
    }

}
