package com.ksyun.campus.day02.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Excel实体BEAN的属性注解
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.FIELD})
public @interface ExcelAnnotation {
    /**
     * Excel列名，表名
     */
    String name();

    /**
     * 列宽
     */
    int width() default 10;

    /**
     * 排序
     */
    int order() default 1;
}