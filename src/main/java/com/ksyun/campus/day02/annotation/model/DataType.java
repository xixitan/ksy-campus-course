package com.ksyun.campus.day02.annotation.model;

import lombok.Data;

@Data
public class DataType {
    //8位：1个符号位+7个数据位
    private byte byteNum;
    //16位：1个符号位+15个数据位
    private short shortNum;
    //32位
    private int intNum;
    //64位
    private long longNum;
    //单精度浮点型：32位，aEb=a×10^b
    private float floutNum;
    //双精度浮点型：64位
    private double doubleNum;
    private char charNum;
    private boolean booleanNum;
    private int[] intArr;
    private Dept dept;


    public static DataType getDefaultMax() {
        DataType maxValue = new DataType();
        maxValue.setByteNum(Byte.MAX_VALUE);
        maxValue.setShortNum(Short.MAX_VALUE);
        maxValue.setIntNum(Integer.MAX_VALUE);
        maxValue.setLongNum(Long.MAX_VALUE);
        maxValue.setFloutNum(Float.MAX_VALUE);
        maxValue.setDoubleNum(Double.MAX_VALUE);
        maxValue.setBooleanNum(Boolean.TRUE);
        maxValue.setIntArr(new int[]{1, 2, 3, 4, 5});
        return maxValue;
    }

    public static DataType getDefaultMin() {
        DataType maxValue = new DataType();
        maxValue.setByteNum(Byte.MIN_VALUE);
        maxValue.setShortNum(Short.MIN_VALUE);
        maxValue.setIntNum(Integer.MIN_VALUE);
        maxValue.setLongNum(Long.MIN_VALUE);
        maxValue.setFloutNum(Float.MIN_VALUE);
        maxValue.setDoubleNum(Double.MIN_VALUE);
        maxValue.setBooleanNum(Boolean.FALSE);
        maxValue.setIntArr(new int[]{1, 2, 3, 4, 5});
        return maxValue;
    }



}
