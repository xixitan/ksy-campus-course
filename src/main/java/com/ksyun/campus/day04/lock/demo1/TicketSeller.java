package com.ksyun.campus.day04.lock.demo1;


public class TicketSeller extends Thread{

    private String sellerName;
    Inventory inventory;

    public TicketSeller(String sellerName, Inventory inventory){
        this.sellerName = sellerName;
        this.inventory = inventory;
    }

    @Override
    public void run()
    {
        while (true){
            if(inventory.ticket > 0) {
                try {
                    Thread.sleep(10);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(sellerName + " 余票还有：" + --inventory.ticket);
            }
            else{
                break;
            }
        }
    }
}