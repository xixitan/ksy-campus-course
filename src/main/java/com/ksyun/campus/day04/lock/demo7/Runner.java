package com.ksyun.campus.day04.lock.demo7;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Runner
{
    static Lock lock = new ReentrantLock();

    public static void main(String[] args)
    {
        Thread thread1 = new Thread(new Thread1());
        Thread thread2 = new Thread(new Thread2());
        thread1.start();
        thread2.start();
    }
}
class Thread1 implements Runnable{

    @Override
    public void run()
    {
        while(true){
            Runner.lock.lock();
            System.out.println("thread1---打印");
            try {
                Thread.sleep(200);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
            Runner.lock.unlock();
            System.out.println("thread1---打印完毕");

        }

    }
}

class Thread2 implements Runnable{

    @Override
    public void run()
    {
        while(true){
            Runner.lock.lock();
            System.out.println("thread2+++打印");
            try {
                Thread.sleep(200);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("thread2+++打印完毕");
            Runner.lock.unlock();
        }
    }
}