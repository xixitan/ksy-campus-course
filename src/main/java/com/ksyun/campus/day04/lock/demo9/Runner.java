package com.ksyun.campus.day04.lock.demo9;

import java.util.concurrent.locks.ReentrantLock;

public class Runner
{
    public static void main(String[] args)
    {
        ReentrantLock lock = new ReentrantLock();

        // 主线程普通加锁
        System.out.println("主线程优先获取锁");
        lock.lock();
        try {
            // 创建子线程
            Thread t1 = new Thread(() -> {
                try {
                    System.out.println("t1尝试获取打断锁");
                    lock.lockInterruptibly();
                } catch (InterruptedException e) {
                    System.out.println("t1没有获取到锁，被打断，直接返回");
                    return;
                }
                try {
                    System.out.println("t1成功获取锁");
                } finally {
                    System.out.println("t1释放锁");
                    lock.unlock();
                }
            }, "t1");
            t1.start();
            try {
                Thread.sleep(2000);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("主线程进行打断锁");
            t1.interrupt();
        } finally {
            // 主线程解锁
            System.out.println("主线程优先释放锁");
            lock.unlock();
        }
    }

}
