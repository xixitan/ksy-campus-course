package com.ksyun.campus.day04.lock.demo6;

import static com.ksyun.campus.day04.lock.demo6.Runner.LOCK;

public class Runner
{
    static final String LOCK = "lock";
    public static void main(String[] args)
    {
        Thread waitThread1 = new Thread(new WaitThread("waitThread1"));
        Thread waitThread2 = new Thread(new WaitThread("waitThread2"));
        Thread notifyThread = new Thread(new NotifyThread());
        waitThread1.start();
        waitThread2.start();
        try {
            //避免notify执行时还没执行wait
            Thread.sleep(1000);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        notifyThread.start();

    }
}

class WaitThread implements Runnable{

    boolean hasWait;
    String name;
    public WaitThread(String name){

        this.name = name;
    }

    @Override
    public void run()
    {
        while(true){
            synchronized(LOCK){
                System.out.println(name + "---打印");
                try {
                    Thread.sleep(200);
                    if(!hasWait){
                        LOCK.wait();
                        hasWait = true;
                    }
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(name + "---打印完毕");
            }
        }
    }
}

class NotifyThread implements Runnable{

    int count = 10;
    @Override
    public void run()
    {
        while(true){
            synchronized(LOCK){

                System.out.println("NotifyThread+++打印");
                try {
                    Thread.sleep(200);
                    if(--count < 0){
//                        LOCK.notify();
                        LOCK.notifyAll();
                        break;
                    }
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("NotifyThread+++打印完毕");
            }
        }
    }
}