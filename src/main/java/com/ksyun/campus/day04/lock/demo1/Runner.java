package com.ksyun.campus.day04.lock.demo1;


public class Runner
{
    public static void main(String[] args)
    {
        Inventory inventory = new Inventory(100);

        TicketSeller ticketSeller = new TicketSeller("1号窗口", inventory);
        ticketSeller.start();

    }

}

