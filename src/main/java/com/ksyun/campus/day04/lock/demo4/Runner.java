package com.ksyun.campus.day04.lock.demo4;

import com.ksyun.campus.day04.lock.demo1.Inventory;

public class Runner
{
    public static void main(String[] args)
    {
        Inventory inventory = new Inventory(100);

        TicketSellerWithLock ticketSeller = new TicketSellerWithLock("1号窗口", inventory);
        ticketSeller.start();

        TicketSellerWithLock ticketSeller2 = new TicketSellerWithLock("2号窗口", inventory);
        ticketSeller2.start();

    }

}

