package com.ksyun.campus.day04.lock.demo2;

import com.ksyun.campus.day04.lock.demo1.Inventory;
import com.ksyun.campus.day04.lock.demo1.TicketSeller;

public class Runner
{
    public static void main(String[] args)
    {
        Inventory inventory = new Inventory(100);

        TicketSeller ticketSeller = new TicketSeller("1号窗口", inventory);
        ticketSeller.start();

        TicketSeller ticketSeller2 = new TicketSeller("2号窗口", inventory);
        ticketSeller2.start();

    }

}

