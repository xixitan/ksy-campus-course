package com.ksyun.campus.day04.lock.demo3;

import com.ksyun.campus.day04.lock.demo1.Inventory;

public class TicketSellerWithLock
        extends Thread{

    private String sellerName;
    Inventory inventory;

    public TicketSellerWithLock(String sellerName, Inventory inventory){
        this.sellerName = sellerName;
        this.inventory = inventory;
    }

    @Override
    public void run()
    {
        while (true){
            synchronized (inventory){
                if(inventory.ticket > 0) {
                    try {
                        Thread.sleep(10);
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(sellerName + " 余票还有：" + --inventory.ticket);
                }
                else{
                    break;
                }
            }
        }
    }
}