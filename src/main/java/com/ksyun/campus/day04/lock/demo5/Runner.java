package com.ksyun.campus.day04.lock.demo5;

import lombok.Synchronized;

import static com.ksyun.campus.day04.lock.demo5.Runner.*;

public class Runner
{
    static final String LOCK = "lock";
    public static void main(String[] args)
    {
        Thread thread1 = new Thread(new Thread1());
        Thread thread2 = new Thread(new Thread2());
        thread1.start();
        thread2.start();

    }
}
class Thread1 implements Runnable{

    boolean hasWait;
    @Override
    public void run()
    {
        while(true){
            synchronized(LOCK){
                System.out.println("thread1---打印");
                try {
                    Thread.sleep(200);
                    if(!hasWait){
                        LOCK.wait(200);
                        hasWait = true;
                    }
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("thread1---打印完毕");
            }
        }
    }
}

class Thread2 implements Runnable{

    @Override
    public void run()
    {
        while(true){
            synchronized(LOCK){

                System.out.println("thread2+++打印");
                try {
                    Thread.sleep(200);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("thread2+++打印完毕");
            }
        }
    }
}