package com.ksyun.campus.day04.lock.demo4;

import com.ksyun.campus.day04.lock.demo1.Inventory;

public class TicketSellerWithLock
        extends Thread{

    static String sellerName;
    static Inventory inventory;

    public TicketSellerWithLock(String sellerName, Inventory inventory){
        this.sellerName = sellerName;
        this.inventory = inventory;
    }

    @Override
    public void run()
    {
        while (true){
            if (!sell(inventory)){
                break;
            }
            try {
                Thread.sleep(10);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static synchronized boolean sell(Inventory inventory){
        if(inventory.ticket > 0) {
            try {
                Thread.sleep(10);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
            //sellerName是静态变量，所以2号窗口的值会覆盖1号窗口
//            System.out.println(sellerName + " 余票还有：" + --inventory.ticket);
            System.out.println(Thread.currentThread().getName() + " 余票还有：" + --inventory.ticket);
            return true;
        }
        else{
            return false;
        }
    }
}