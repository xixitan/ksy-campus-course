package com.ksyun.campus.day04.threads.create;

public class MyThread {
  public static void main(String[] args) {

    Thread t = new ThreadDemo();

    t.start();

    for (int i = 0; i < 5; i++) {
      System.out.println("first output：" + i);
    }
  }
}

class ThreadDemo extends Thread{
  @Override
  public void run() {
    for (int i = 0; i < 5; i++) {
      System.out.println("second output：" + i);
    }
  }
}
