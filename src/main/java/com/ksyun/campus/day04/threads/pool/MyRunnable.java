package com.ksyun.campus.day04.threads.pool;

public class MyRunnable implements Runnable{
  @Override
  public void run() {
    for (int i = 0; i < 5; i++) {
      System.out.println(Thread.currentThread().getName() + " output: "  + i);
    }
    try {
      System.out.println(Thread.currentThread().getName() + "sleep zzz~~~");
      Thread.sleep(10000000);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
