package com.ksyun.campus.day04.threads.timer;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class TimerDemo {
  public static void main(String[] args) {

    Timer timer = new Timer();

    timer.schedule(new TimerTask() {
      @Override
      public void run() {
        System.out.println(Thread.currentThread().getName() + "output:1 =>" + new Date());
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
      }
    }, 0, 1000);

    timer.schedule(new TimerTask() {
      @Override
      public void run() {
        System.out.println(Thread.currentThread().getName() + "output:2 =>"+ new Date());
        // 执行下面异常代码，影响其他定时任务。
//        System.out.println(9/0);
      }
    }, 0, 1000);

    timer.schedule(new TimerTask() {
      @Override
      public void run() {
        System.out.println(Thread.currentThread().getName() + "output:3 =>"+ new Date());
      }
    }, 0, 1000);
  }
}
