package com.ksyun.campus.day04.threads.pool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FixedThreadPool {
  public static void main(String[] args) throws Exception {

    ExecutorService pool = Executors.newFixedThreadPool(3);

    pool.execute(new MyRunnable());
    pool.execute(new MyRunnable());
    pool.execute(new MyRunnable());
    pool.execute(new MyRunnable());
  }
}
