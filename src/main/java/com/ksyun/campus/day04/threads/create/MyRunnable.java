package com.ksyun.campus.day04.threads.create;

public class MyRunnable {
  public static void main(String[] args) {

    Runnable target = new RunnableDemo();
    Thread t = new Thread(target);
    t.start();

    for (int i = 0; i < 10; i++) {
      System.out.println("first output：" + i);
    }
  }
}

class RunnableDemo implements Runnable {

  @Override
  public void run() {
    for (int i = 0; i < 10; i++) {
      System.out.println("second output：" + i);
    }
  }
}
