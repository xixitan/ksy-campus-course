package com.ksyun.campus.day04.threads.api;

public class ThreadName{

  public static void main(String[] args) {
    Thread t1 = new ThreadDemo("one");
    t1.start();
    System.out.println(t1.getName());

    Thread t2 = new ThreadDemo("two");
    t2.start();
    System.out.println(t2.getName());

    Thread m = Thread.currentThread();
    System.out.println(m.getName());
    m.setName("setname");

    for (int i = 0; i < 5; i++) {
      System.out.println( m.getName() + " output：" + i);
    }
  }
}

class ThreadDemo extends Thread{

  public ThreadDemo(String name) {
    super(name);
  }

  @Override
  public void run() {
    for (int i = 0; i < 5; i++) {
      System.out.println( Thread.currentThread().getName() + " out：" + i);
    }
  }
}
