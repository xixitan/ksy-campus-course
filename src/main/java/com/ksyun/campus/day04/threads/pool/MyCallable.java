package com.ksyun.campus.day04.threads.pool;

import java.util.concurrent.Callable;

public class MyCallable implements Callable<String> {

  private final int n;

  public MyCallable(int n) {
    this.n = n;
  }

  @Override
  public String call() {
    int sum = 1;
    for (int i = 1; i <= n ; i++) {
      sum *= i;
    }
    return Thread.currentThread().getName()
        + " " + n+ "! =" + sum;
  }
}
