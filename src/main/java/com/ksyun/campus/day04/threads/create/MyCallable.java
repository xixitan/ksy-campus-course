package com.ksyun.campus.day04.threads.create;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

public class MyCallable {
  public static void main(String[] args) {

    Callable<String> call = new CallableDemo(10);

    FutureTask<String> f1 = new FutureTask<>(call);

    Thread t1 = new Thread(f1);

    t1.start();

    Callable<String> call2 = new CallableDemo(20);
    FutureTask<String> f2 = new FutureTask<>(call2);
    Thread t2 = new Thread(f2);
    t2.start();

    try {
      String rs1 = f1.get();
      System.out.println("first " + rs1);
    } catch (Exception e) {
      e.printStackTrace();
    }

    try {
      String rs2 = f2.get();
      System.out.println("second " + rs2);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}

class CallableDemo implements Callable<String>{

  private final int n;

  public CallableDemo(int n) {
    this.n = n;
  }

  @Override
  public String call() {
    int sum = 0;
    for (int i = 1; i <= n ; i++) {
      sum += i;
    }
    return "result：" + sum;
  }
}
