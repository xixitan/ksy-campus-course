package com.ksyun.campus.day04.threads.pool;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadPoolCallable {
  public static void main(String[] args) throws Exception {

    ExecutorService pool = new ThreadPoolExecutor(3, 5 ,
        6, TimeUnit.SECONDS, new ArrayBlockingQueue<>(5) ,
        Executors.defaultThreadFactory(), new ThreadPoolExecutor.AbortPolicy() );

    Future<String> f1 = pool.submit(new MyCallable(3));
    Future<String> f2 = pool.submit(new MyCallable(4));
    Future<String> f3 = pool.submit(new MyCallable(5));
    Future<String> f4 = pool.submit(new MyCallable(6));
    Future<String> f5 = pool.submit(new MyCallable(7));

    System.out.println(f1.get());
    System.out.println(f2.get());
    System.out.println(f3.get());
    System.out.println(f4.get());
    System.out.println(f5.get());

    pool.shutdown();
  }
}
