package com.ksyun.campus.day04.threads.pool;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadPoolRunnable {
  public static void main(String[] args) {

    ExecutorService pool = new ThreadPoolExecutor(3, 5 ,
        6, TimeUnit.SECONDS, new ArrayBlockingQueue<>(5) ,
        Executors.defaultThreadFactory(),
        new ThreadPoolExecutor.AbortPolicy() );

    Runnable target = new MyRunnable();
    pool.execute(target);
    pool.execute(target);
    pool.execute(target);

    pool.execute(target);
    pool.execute(target);
    pool.execute(target);
    pool.execute(target);
    pool.execute(target);

    pool.execute(target);
    pool.execute(target);

    // 等待全部任务执行完毕之后再关闭（建议使用的）
    pool.shutdown();
  }
}
