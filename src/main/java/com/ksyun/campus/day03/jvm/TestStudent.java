package com.ksyun.campus.day03.jvm;

public class TestStudent {
    public static void main(String[] args) {
        Student student = new Student();
        // 打印 student 对象的内存地址
        System.out.println(student);
        // 打印成员变量的默认值
        System.out.println(student.name + " " + student.age);
        student.name = "小明";
        student.age = 20;
        // 打印成员变量新值
        System.out.println(student.name + " " + student.age);
    }
}
