package com.ksyun.campus.day03.memleak;

public class StackOverflowDemo {
    public int count = 0;
    public static void main(String[] args) {
        StackOverflowDemo demo = new StackOverflowDemo();
        demo.walk();
    }
    public void walk() {
        count++;
        System.out.println(count);
        walk();
    }
}
