package com.ksyun.campus.day03.memleak;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class ThreadLocalDemo1 {

    public static ThreadLocal<Session> threadLocal = new ThreadLocal<>();

    public static AtomicInteger id = new AtomicInteger();

    public void execute() {
        startSession();
        doWork();
    }

    public void startSession () {
        threadLocal.set(new Session(id.incrementAndGet()));
    }

    public void doWork() {
        Session session = threadLocal.get();
        System.out.println("done, id:" + session.getId() + ", space size: " + session.getSpace().length);
        session = null;
    }

    public static void main(String[] args) throws Exception {
        ExecutorService executor = Executors.newFixedThreadPool(10);
        for (int i=0;i<10;i++) {
            executor.execute(() -> new ThreadLocalDemo1().execute());
        }
        // 等待任务全部执行完成
        Thread.sleep(2000);
        // 线程执行任务后，销毁threadLocal
        threadLocal = null;
        // 模拟程序不退出
        Thread.sleep(100000);
    }
}
