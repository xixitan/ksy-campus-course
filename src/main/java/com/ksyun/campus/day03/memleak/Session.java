package com.ksyun.campus.day03.memleak;

public class Session {

    private Integer id;

    private byte[] space;

    public Session(Integer id) {
        this.id =  id;
        this.space = new byte[1024 * 1024 * 20];
    }

    public Integer getId() {
        return this.id;
    }

    public byte[] getSpace() {
        return this.space;
    }
}
