package com.ksyun.campus.day03.gc;

public class Student {
    public String name;
    public int age;
    public Student friend;
    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }
    public static void main(String[] args) {
        Student zhangsan = new Student("张三", 20);
        zhangsan = null;
        Student lisi = new Student("李四", 21);
        Student wangwu = new Student("王五", 22);
        lisi.friend = wangwu;
        wangwu = null;
        System.out.println(lisi.friend.name);
    }
}
