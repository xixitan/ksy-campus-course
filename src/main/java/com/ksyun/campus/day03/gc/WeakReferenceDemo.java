package com.ksyun.campus.day03.gc;

import java.lang.ref.WeakReference;

public class WeakReferenceDemo {

    public static void main(String[] args) {
        // 强引用
        Object o = new Object();
        System.out.println(o);
        // 弱引用，此时对象通过强引用o以及弱引用weakReference可达
        WeakReference<Object> weakReference = new WeakReference<>(o);
        System.out.println("通过弱引用获取对象:" + weakReference.get());
        // 移除强引用，只剩下弱引用
        o = null;
        // 手动触发GC, 仅用于学习，实际生产中无须手动GC
        System.gc();
        // 即使内存充足， 对象也被GC回收
        System.out.println("GC后通过弱引用获取对象:" + weakReference.get());
    }
}
