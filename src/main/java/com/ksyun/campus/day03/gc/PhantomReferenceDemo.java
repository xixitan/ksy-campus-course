package com.ksyun.campus.day03.gc;

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;

public class PhantomReferenceDemo {

    public static void main(String[] args) {
        // 强引用
        Object o = new Object();
        System.out.println(o);
        PhantomReference<Object> phantomReference = new PhantomReference<>(o, new ReferenceQueue<>());
        System.out.println("通过虚引用获取对象:" + phantomReference.get());
        // 移除强引用，只剩下虚引用
        o = null;
    }
}
