package com.ksyun.campus.day03.gc;

import java.lang.ref.SoftReference;

public class SoftReferenceDemo {

    public static void main(String[] args) {
        // 强引用
        Object o = new Object();
        System.out.println(o);
        // 软引用，此时对象通过强引用o以及软引用softReference可达
        SoftReference<Object> softReference = new SoftReference<>(o);
        System.out.println("通过软引用获取对象:" + softReference.get());
        // 移除强引用，只剩下软引用
        o = null;
        // 手动触发GC, 仅用于学习，实际生产中无须手动GC
        System.gc();
        // 对象依然存在
        System.out.println("GC后通过软引用获取对象:" + softReference.get());
    }
}
