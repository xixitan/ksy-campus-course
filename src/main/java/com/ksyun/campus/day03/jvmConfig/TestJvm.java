package com.ksyun.campus.day03.jvmConfig;

import java.util.ArrayList;
import java.util.List;

public class TestJvm {
    public static List<Student> StudentList = new ArrayList<>();
    public static void main(String[] args) throws InterruptedException {
        for (int i = 0;i < 1000;i++) {
            Student student = new Student("小明", 20, "一班");
            StudentList.add(student);
            Thread.sleep(2000);//暂停2s ,测试堆栈内存可放到循环外.
        }
    }

}

class Student{
    public Student(String name, int age, String classS) {
        this.name = name;
        this.age = age;
        this.classS = classS;
    }

    String name;
    int age;
    String classS;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getClassS() {
        return classS;
    }

    public void setClassS(String classS) {
        this.classS = classS;
    }
}
