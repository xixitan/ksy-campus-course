package com.ksyun.campus.day03.homework;

public class RuntimeDemo {

    /**
     * 可设定不同参数-Xms10M -Xmx100M运行
     * @param args
     */
    public static void main(String[] args) {
        Runtime runtime = Runtime.getRuntime();
        long total = runtime.totalMemory();
        long free = runtime.freeMemory();
        long max = runtime.maxMemory();

        System.out.println("最大堆内存(M):" + max / (1024 * 1024));
        System.out.println("已使用堆内存(M):" + (total - free) / (1024 * 1024));
    }
}
