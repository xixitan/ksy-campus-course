package com.ksyun.campus.day03.homework;

public class JVMDemo {

    /**
     * 设定启动参数: -Xms20M -Xmx20M -XX:+PrintGCDetails -Xmn10M
     * 设置年轻代大点即可将新生对象分配在年轻代中
     * @param args
     */
    public static void main(String[] args) {
        byte[] b1, b2, b3, b4;
        b1 = new byte[1024 * 1024];
        b2 = new byte[1024 * 1024];
        b3 = new byte[1024 * 1024];
        b4 = new byte[1024 * 1024];
    }
}
