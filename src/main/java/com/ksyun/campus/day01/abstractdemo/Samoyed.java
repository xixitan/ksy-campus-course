package com.ksyun.campus.day01.abstractdemo;

/**
 * <p>
 * 萨摩耶
 * </p>
 */
public class Samoyed extends WhiteDog {

    @Override
    public int bodyLength() {
        return 1;
    }

    public static void main(String[] args) {
        Samoyed dog1 = new Samoyed();
        System.out.println("dog1.furColor():" + dog1.furColor());
        System.out.println("dog1.bodyLength()" + dog1.bodyLength());

        // 抽象类与接口一样都不能进行实例化
        // WhiteDog dog2 = new WhiteDog(); // compile error
    }
}
