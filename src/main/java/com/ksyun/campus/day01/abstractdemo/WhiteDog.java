package com.ksyun.campus.day01.abstractdemo;

// 抽象类需要用abstract关键字标记
public abstract class WhiteDog implements Dog {

    // 抽象类中的抽象方法
    public abstract int bodyLength();

    // 抽象类中也可以有具体方法
    @Override
    public String furColor() {
        return "white";
    }

}
