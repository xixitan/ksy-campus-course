package com.ksyun.campus.day01.abstractdemo;

public interface Dog {
    String furColor();
}
