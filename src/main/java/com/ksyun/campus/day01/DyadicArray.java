package com.ksyun.campus.day01;

public class DyadicArray {
    public static void main(String[] args) {
        int[][] myNumbers = {{1, 2, 3, 4}, {5, 6, 7}};
        System.out.println("myNumbers[1][2]=" + myNumbers[1][2]); // 输出 7

        System.out.println("二维数组的元素有：");
        for (int[] subArr : myNumbers) {
            for (int x : subArr) {
                System.out.print(x + " ");
            }
            System.out.println();
        }
    }
}
