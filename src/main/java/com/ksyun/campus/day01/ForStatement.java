package com.ksyun.campus.day01;

public class ForStatement {

    public static void main(String[] args) {
        int sum1 = 0;
        for (int i = 0; i < 10; i++) {
            sum1 += i;
        }

        System.out.println("sum1=" + sum1);

        int[] array = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int sum2 = 0;
        for (int x : array) {
            sum2 += x;
        }

        System.out.println("sum2=" + sum2);
    }
}
