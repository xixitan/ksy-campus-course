package com.ksyun.campus.day01;

public class DemoClass1 {

    private int field1; // 上文提到过的实例变量
    private String field2;

    // 类中的方法
    public int getResult(int factor) {
        return this.field1 * factor;
    }
}
