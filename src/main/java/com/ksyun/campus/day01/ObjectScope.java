package com.ksyun.campus.day01;

public class ObjectScope {
    public static void func1() {
        int a = 1;
        {
            int b = 2; // 变量b的作用域只在该大括号中
        }

        System.out.println("a=" + a); // a仍在作用域之内
        // System.out.println("b=" + b); // compile error!
    }

    public static void func2() {
        int a = 1;
        {
            // int a = 2; // compile error! java不允许变量覆盖
        }

        System.out.println("a=" + a); // a仍在作用域之内
    }
}
