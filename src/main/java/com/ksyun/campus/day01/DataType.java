package com.ksyun.campus.day01;

public class DataType {
    public static void main(String[] args) {

        // 整数类型：byte,short,int,long
        byte a = 1;
        short b = 2;
        int c = 3;
        long d = 4L; // 整数字面量默认为int类型，在其后增加后缀L表示long类型

        // 浮点数类型:float,double
        float e = 5.5F; // 浮点数字面量默认为double类型，在其后增加F后缀表示float类型
        double f = 6.6;

        boolean g = true;

        int[] h = new int[] {1, 2, 3};
        Object i = new Object();

        System.out.println("a=" + a);
        System.out.println("b=" + b);
        System.out.println("c=" + c);
        System.out.println("d=" + d);
        System.out.println("e=" + e);
        System.out.println("f=" + f);
        System.out.println("g=" + g);
        System.out.println("h=" + h);
        System.out.println("i=" + i);
    }
}
