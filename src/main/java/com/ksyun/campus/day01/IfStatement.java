package com.ksyun.campus.day01;

public class IfStatement {
    public static void main(String[] args) {
        int a = 10;
        int b = 11;
        if (a >= b) {
            System.out.println("a >= b");
        } else {
            System.out.println("a < b");
        }


        int c = 40;
        if (c > 60) {
            System.out.println("x的值大于60");
        } else if (c > 30) {
            System.out.println("x的值大于30但小于60");
        } else if (c > 0) {
            System.out.println("x的值大于0但小于30");
        } else {
            System.out.println("x的值小于等于0");
        }
    }
}
