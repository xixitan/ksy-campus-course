package com.ksyun.campus.day01.inherit;

public class Circle {
    private double x;

    public static Circle create() {
        // this.getX(); // compile error!
        // this.x = 0; // compile error!
        return new Circle();
    }

    public double getX() {
        Circle c = create(); // OK
        return c.x;
    }
}
