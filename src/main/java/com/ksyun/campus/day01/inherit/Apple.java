package com.ksyun.campus.day01.inherit;

public class Apple extends Fruit {
    @Override
    public void eat() {
        super.eat(); // 可以使用super关键次调用父类的非private方法或引用父类的非private属性
        System.out.println("eat Apple");
    }

    public static void main(String[] args) {
        Fruit fruit1 = new Fruit();
        fruit1.eat();
        System.out.println("-----\n");

        Apple apple = new Apple();
        apple.eat();
        System.out.println("-----\n");

        // 注意此行与第11行的不同
        Fruit fruit2 = new Apple();
        fruit2.eat();

    }
}
