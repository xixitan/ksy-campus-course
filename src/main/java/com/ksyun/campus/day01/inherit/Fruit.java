package com.ksyun.campus.day01.inherit;

public class Fruit {
    int num;

    public void eat() {
        System.out.println("eat Fruit with number:" + num);
    }

}
