package com.ksyun.campus.day01.inherit;

// 使用extends关键字使Son继承自Father
public class Son extends Father {

    public static void main(String[] args) {
        Son son = new Son();
        son.feature();  // Son自动继承了Father的feature()方法
    }
}
