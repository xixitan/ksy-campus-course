package com.ksyun.campus.day01;

public class WhileStatement {
    public static void main(String[] args) {
        int count = 0;
        while (count < 10) {
            count++;
        }
        System.out.println("count=" + count);

        // do...while至少会执行一次循环体
        count = 0;
        do {
            count++;
        } while (count > 10);
        System.out.println("count=" + count);
    }
}
