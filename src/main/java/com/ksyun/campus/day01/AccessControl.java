package com.ksyun.campus.day01;

public class AccessControl {
    private int privateField; // 仅在当前类中可以访问
    int defaultField; // 当前类和同一包下的类可以访问
    protected int protectedField; // 当前类，同一包下的类，当前类的子类可以访问

    public int publicField; // 所有类均可访问

    private void privateFunc() {}

    void defaultFunc() {}

    protected void protectedFunc() {}

    public void publicFunc() {}

}


class Client {

    public void tryAccess() {
        AccessControl ac = new AccessControl();
        // System.out.println(ac.privateField); // compile error!

        System.out.println(ac.defaultField); // OK
        System.out.println(ac.protectedField); // OK
        System.out.println(ac.publicField); // OK

        // ac.privateFunc(); //compile error!
        ac.defaultFunc(); // OK
        ac.protectedFunc(); // OK
        ac.publicFunc(); // OK
    }
}
