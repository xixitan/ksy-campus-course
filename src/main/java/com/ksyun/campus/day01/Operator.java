package com.ksyun.campus.day01;

public class Operator {
    public static void main(String[] args) {

        // 算术运算符
        int a = (2 + 2) * (3 / 3) - 5;
        a++;
        System.out.println("1:a=" + a);

        // 逻辑运算符，短路与
        if (a > 0 && a++ > 0) {
            System.out.println("2:a=" + a);
        }
        System.out.println("3:a=" + a);

        // 逻辑运算符，非短路
        if (a > 0 & a++ > 0) {
            System.out.println("4:a=" + a);
        }
        System.out.println("5:a=" + a);

        // 移位运算符
        int b = 4;
        b = b >> 1;
        System.out.println("1:b=" + b);

        b = b << 2;
        System.out.println("2:b=" + b);

        // 三元运算符
        boolean c = b < 10 ? true : false;
        System.out.println("c=" + c);
    }
}
