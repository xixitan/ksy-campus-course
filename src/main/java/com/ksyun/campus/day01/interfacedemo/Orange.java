package com.ksyun.campus.day01.interfacedemo;

public class Orange implements Fruit, Sphere {
    private double volume;

    public Orange(double volume) {
        this.volume = volume;
    }

    /**
     * 类中必须为所有接口的方法都提供实现方法才行
     */
    @Override
    public boolean isTasty() {
        return true;
    }

    @Override
    public double getVolume() {
        return this.volume;
    }

    @Override
    public String toString() {
        return "volume is:" + this.volume;
    }

    public static void main(String[] args) {
        Orange orange1 = new Orange(11);
        System.out.println("orange1:" + orange1);
        System.out.println("orange1.isTasty():" + orange1.isTasty()); // 从Fruit接口继承的isTasty()方法
        System.out.println("orange1.getVolume():" + orange1.getVolume()); // 从Sphere接口继承的getVolume()方法

        // 可以使用接口类型的引用来指向Orange对象
        Fruit orange2 = new Orange(22);
        Sphere ornage3 = new Orange(33);

        // 接口不可以被实例化
        // Fruit fruit = new Fruit(); //compile error!
        // Sphere sphere = new Sphere(); // compile error!
    }
}
