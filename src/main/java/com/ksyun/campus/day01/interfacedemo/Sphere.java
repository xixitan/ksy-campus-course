package com.ksyun.campus.day01.interfacedemo;

interface Sphere {
    double getVolume();
}
