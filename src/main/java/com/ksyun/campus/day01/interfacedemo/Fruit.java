package com.ksyun.campus.day01.interfacedemo;

public interface Fruit {
    boolean isTasty();
}
