package com.ksyun.campus.day01.exception;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class TryWithResource {

    public static void main(String[] args) {
        File file = new File("C:\\abc");
        try (InputStream in = new FileInputStream(file)) {
            int x = in.read();
        } catch (IOException e) {
            System.out.println("读取文件时发生了异常");
            e.printStackTrace();
        }
    }
}
