package com.ksyun.campus.day01.exception;

public class MultiCatch {

    public static void main(String[] args) {
        try {
            int x = 1 / 0;
            int y = Integer.parseInt("abc");
        } catch (ArithmeticException | NumberFormatException e) {
            // 捕获并处理try抛出的异常类型
            e.printStackTrace();
        } finally {
            System.out.println("不管是否出现异常，都执行此代码");
        }

    }
}
