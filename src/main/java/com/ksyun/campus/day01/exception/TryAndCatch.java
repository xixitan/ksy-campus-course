package com.ksyun.campus.day01.exception;

public class TryAndCatch {
    static void cacheException() throws Exception {
        try {
            System.out.println("invoked try...");
            System.out.println(1 / 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        cacheException();
    }
}
