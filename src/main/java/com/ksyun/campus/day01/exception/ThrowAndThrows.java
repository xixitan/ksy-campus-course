package com.ksyun.campus.day01.exception;

public class ThrowAndThrows {
    static void cacheException() throws Exception {
        throw new Exception();
    }

    public static void main(String[] args) {

        /**
         * 下面这个调用将会导致编译错误, </br>
         * 因为Exception类不属于RuntimeException， </br>
         * 所以调用方必须捕获该异常，或者使用throws语句
         */
        // cacheException(); // compile error
    }
}
