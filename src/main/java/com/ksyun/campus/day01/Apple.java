package com.ksyun.campus.day01;

public class Apple {
    int sum;
    String color;

    public Apple() {}

    public Apple(int sum) {
        this.sum = sum;
    }

    public Apple(String color) {
        this.color = color;
    }

    public Apple(int sum, String color) {
        this.sum = sum;
        this.color = color;
    }

    /**
     * getApple方法是重载方法
     * 
     * @param num
     * @return
     */
    public int getApple(int num) {
        return sum + num;
    }

    public String getApple(String color) {
        return color;
    }

    /**
     * add方法是一个可变参数列表的方法
     * 
     * @param numbers
     * @return
     */
    public int add(int... numbers) {
        int sum = 0;
        for (int num : numbers) {
            sum += num;
        }
        return sum;
    }

    public String toString() {
        return "sum=" + sum + ",color=" + color;
    }

    public static void main(String[] args) {
        Apple apple1 = new Apple();
        Apple apple2 = new Apple(1);
        Apple apple3 = new Apple("red");
        Apple apple4 = new Apple(2, "color");

        System.out.println("apple1 : " + apple1);
        System.out.println("apple2 : " + apple2);
        System.out.println("apple3 : " + apple3);
        System.out.println("apple4 : " + apple4);

        System.out.println("public int getApple(int num):" + apple1.getApple(10));
        System.out.println("public String getApple(String color):" + apple1.getApple("red"));

        System.out.println("public int add():" + apple1.add());
        System.out.println("public int add(1):" + apple1.add(1));
        System.out.println("public int add(2,1):" + apple1.add(2, 1));
        System.out.println("public int add(new int[] {1,3,2}):" + apple1.add(new int[] {1, 3, 2}));
    }

}
