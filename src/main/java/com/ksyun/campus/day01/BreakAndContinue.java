package com.ksyun.campus.day01;

public class BreakAndContinue {
    public static void main(String[] args) {
        int sum = 0;
        for (int i = 1; i <= 20; i++) {
            if (i % 2 == 0) {
                continue;
            }

            if (i > 10) {
                break;
            }
            sum += i;
        }

        System.out.println("sum=" + sum);
    }
}
