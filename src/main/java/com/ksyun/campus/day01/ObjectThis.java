package com.ksyun.campus.day01;

public class ObjectThis {
    private int i = 0;

    public ObjectThis() {
        this(0); // 此处this的用处是调用其他构造器，this语句需要是该构造器的第一条语句
    }

    public ObjectThis(int i) {
        this.i = i; // 此处的this表示的是当前对象本身
    }

    public ObjectThis eatApple() {
        this.count(); // 此处的this表示的是当前对象本身
        return this;
    }

    public void count() {
        i++;
    }

    public String toString() {
        return "i=" + i;
    }

    public static void main(String[] args) {
        ObjectThis apple = new ObjectThis();
        apple.eatApple().eatApple();
        System.out.println(apple);
    }

}
