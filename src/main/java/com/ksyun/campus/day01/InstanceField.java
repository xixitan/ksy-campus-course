package com.ksyun.campus.day01;

public class InstanceField {
    public String name; // 这个实例变量对所有其他类可见
    private double salary; // 私有变量，仅在该类可见

    public InstanceField(String empName) {
        // 在构造器中对name赋值
        name = empName;
    }

    // 设定salary的值
    public void setSalary(double empSal) {
        salary = empSal;
    }

    // 打印信息
    public void printEmp() {
        System.out.println("名字 : " + name);
        System.out.println("薪水 : " + salary);
    }

    public static void main(String[] args) {
        InstanceField empOne = new InstanceField("RUNOOB");
        empOne.setSalary(1000.0);
        empOne.printEmp();
    }
}
