package com.ksyun.campus.day01.finaldemo;

public final class Apple extends Fruit {
    private final String color = "red";
    private static final boolean TASTY = true;
    
    // @Override
    // public final boolean isFruit() { // compile error!
    // }
}
