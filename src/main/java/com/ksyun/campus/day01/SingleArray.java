package com.ksyun.campus.day01;

public class SingleArray {
    public static void main(String[] args) {
        String[] cars = {"Volvo", "BMW", "Ford", "Mazda"};
        System.out.println("1: cars[0]=" + cars[0]);// 输出 Volvo

        cars[0] = "Opel";
        System.out.println("2: cars[0]=" + cars[0]);// 现在输出 Opel 而不是 Volvo

        System.out.println("cars.length=" + cars.length);// 输出 4

        System.out.println("数组中的元素有：");
        for (String x : cars) {
            System.out.println(x);
        }
    }
}
