package com.ksyun.campus.limiting;

import java.util.Random;

public class TokenBucket {

  static long cur = 0; // 当前桶中令牌数量
  // 记录上次统计时间的毫秒
  static long lastTime = System.currentTimeMillis();

  static boolean TokenBucket(int capcity, int rate) {
    // 当前毫秒
    long millisTime = System.currentTimeMillis();
    // 两次请求的间隔，单位是毫秒
    long time = (millisTime - lastTime);
    // 当前桶中的令牌数
    cur = Math.min(capcity, cur + time * rate);
    lastTime = millisTime;

    if (cur == 0) {
      // 没有令牌拿
      return true;
    }
    // 拿走一块令牌
    --cur;
    return false;
  }

  public static void main(String[] args) {
    for (; ; ) {
      // 停顿1秒
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }

      // 模拟1秒请求的次数 150次内
      int randomTime = new Random().nextInt(150);  // 通过调这个值，来测试是否限流
      for (int j = 0; j < randomTime; j++) {
        request();
      }
    }
  }

  private synchronized static void request() {
    if (TokenBucket(100, 1)) {
      System.out.println("限流了" + cur);
    } else {
      System.out.println("没限流" + cur);
    }
  }

}
