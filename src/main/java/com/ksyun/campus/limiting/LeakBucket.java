package com.ksyun.campus.limiting;

import java.util.Random;

public class LeakBucket {

  static long cur = 0; // 当前桶中累计的请求数
  // 记录上次统计时间的毫秒
  static long lastTime = System.currentTimeMillis();

  static boolean leakBuck(int capcity, int rate) {
    // 当前毫秒
    long millisTime = System.currentTimeMillis();
    // 两次请求的间隔，单位是毫秒
    long time = (millisTime - lastTime);
    cur = Math.max(0, cur - time * rate);
    lastTime = millisTime;
    ++cur;
    if (cur >= capcity) {
      cur = capcity;
      return true;
    }
    return false;
  }

  public static void main(String[] args) {
    for (; ; ) {
      // 停顿1秒
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }

      // 模拟1秒请求的次数 150次内
      int randomTime = new Random().nextInt(150);  // 通过调这个值，来测试是否限流
      for (int j = 0; j < randomTime; j++) {
        if (leakBuck(100, 1)) {
          System.out.println("限流了" + cur);
        } else {
          System.out.println("没限流" + cur);
        }
      }
    }
  }
}
