package com.ksyun.campus.rabbitmq;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class ConsumerTest {
  public static void main(String[] args) throws IOException, TimeoutException {
    // 1.建立连接
    ConnectionFactory factory = new ConnectionFactory();
    // 1.1.设置连接参数，分别是：主机名、端口号、vhost、用户名、密码
    factory.setHost("127.0.0.1");
    factory.setPort(5672);
    factory.setVirtualHost("/");
    factory.setUsername("guest");
    factory.setPassword("guest");
    // 1.2.建立连接
    Connection connection = factory.newConnection();
    // 2.创建通道Channel
    Channel channel = connection.createChannel();
    // 3.创建队列
    String queueName = "ks.queue";
    channel.queueDeclare(queueName, false, false, false, null);
    // 4.订阅消息 (第二个参数 是否自动ack)
    channel.basicConsume(queueName, true, new DefaultConsumer(channel){
      @Override
      public void handleDelivery(String consumerTag, Envelope envelope,
                                 AMQP.BasicProperties properties, byte[] body) throws IOException {
        // 5.处理消息
        String message = new String(body);
        // 此处抛异常 用于验证manual ack
//        int i = 1 / 0;
        System.out.println("接收到消息：【" + message + "】");

        // manual ACK
//        channel.basicAck(envelope.getDeliveryTag(), false);
      }
    });
    System.out.println("等待接收消息。。。。");
  }
}
